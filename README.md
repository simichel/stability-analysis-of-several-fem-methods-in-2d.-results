# Stability analysis of several FEM methods in 2D. Results

This is the public repository refering to the article [arXiv prepint](.........) where we show the all the plots resulting from the stability analysis and the code used to produce those pictures, included the ones not available in the paper.

It was impossible to include all the figures on the dispersion analysis and the simulations we run in [arXiv prepint](https://arxiv.org/abs/2103.16158). 

We run for the different elements, stabilizations, time integration methods, or with the parameters found with different optimization strategies.
In the file names, we use the code :
* `ELEMENT` to denote the different elements between `lagrange` (basic), `cohen` or `cubature` (cubature), `bezier` or `bernstein` (Bernstein)
* `STABILIZATION` to denote the stabilization technique between `OSS`, `CIP`, `SUPG`, `any` (no stabilization)
* `TIME` for time discretization among `RK`, `SSPRK` and `DeC` (only for `cohen`, `bezier` elements)

Figures of the stability analysis
========

The dispersion analysis is performed considering two mesh configurations:
* `MESH` as `meshX` (at left), `meshT` (at right) and `combined_mesh` which corresponds to results combining the most restrictive mesh (contains only the [stabilizationCFLplane](/images/StabilityAnalysis/combined_mesh/FullyDiscrete/stabilizationCFLplane) directory). 


<img src="/images/StabilityAnalysis/meshX/gridX9_P1_lagrange.png" alt="drawing" width="49%"/>
<img src="/images/StabilityAnalysis/meshT/gridT9_P1_lagrange.png" alt="drawing" width="49%"/>

The optimization technique used optimizes the error of the solution η_u. These are two different visualisation of results:
* `PLOT` describe the visualisation choosed, `error` for the dispersive error, `epsilon_contour` for the contour representation of maximum value of the Damping which as to be negative to ensure the stability. Considering the machine precision, the plot show max(1e-16,Ɛ)

For the optimization of (CFL, τ), the values are ploted using the most restrictive results, comparing with different wave angles. 
For dispersion plots using selected coefficients, the waves angles are choosed from π/4 to 3π/4 as : 
* `ANGLE = kπ/8`, k=2,3,4,5,6.

When not specified, the computations are done with the values found with the optimization of η_u. If there is no stable scheme in the (CFL, τ) the plots are performed with by default (CFL, τ) = (1/(2p+1), 0.1) where p is the degree of elements. 

They can be found in this repository in folder [StabilityAnalysis](/images/StabilityAnalysis) organized as follows.

1. Stability analysis plots for linear advection problem using the X mesh in [images/StabilityAnalysis/meshX](/images/StabilityAnalysis/meshX)
    1. Spatial discretization analysis with optimal parameters found [SpatiallyDiscrete](/images/StabilityAnalysis/meshX/SpatiallyDiscrete). Files are denoted by `disc_space_ELEMENT_STABILIZATION_phase_OPTIMIZATION_ANGLES.pdf`
    1. Fully discretization analysis [FullyDiscrete](/images/StabilityAnalysis/meshX/FullyDiscrete). 
        1. The comparison of the result of the optimization techniques on the plane (CFL, τ) are in [stabilizationCFLplane](/images/StabilityAnalysis/meshX/FullyDiscrete/stabilizationCFLplane). Files are denoted as `PLOT_disp_cfl_tau_ELEMENT_TIME_STABILIZATION.pdf`
        1. The dispersion and damping coefficients with respect to the phase for the found parameters are in [dispersionSelectedParameter](/images/StabilityAnalysis/meshX/FullyDiscrete/dispersionSelectedParameters). Files are denoted by `disp_full_rescale_OPTIMIZATION_ELEMENT_TIME_STABILIZATION_phase_ANGLES.pdf`



Figures of the convergence tests
========

The convergence tests are performed considering two mesh configurations:
* `MESH` as `meshX`, `unstructured_mesh` 
* `ELEMENT` to denote the different elements between `lagrange` (basic), `cohen` (cubature), `bezier` (Bernstein)
* `STABILIZATION` to denote the stabilization technique between `OSS`, `CIP`, `SUPG`
* `TIME` for time discretization among `SSPRK` (for `lagrange` and `cohen` elements) and `DeC` (for `cohen` and `bezier` elements)


1. Simulation error results using the X mesh are in [Simulations/meshX](/images/Simulations/meshX).
    1. Linear advection tests are in [linearAdvection](/images/Simulations/meshX/linearAdvection)
        1. The convergence plots **time step** vs **error** are in [convergence](/images/Simulations/meshX/linearAdvection/convergence) and are denoted by `2D_meshX_LinearAdvection_TIME_ELEMENT_STABILIZATION.pdf`
        1. The convergence plots **computational time** vs **error** are in [timeErrorPlot](/images/Simulations/meshX/linearAdvection/timeErrorPlot) and files comparing the stabilization techniques are denoted by `2D_meshX_timeError_LinearAdvection_TIME_ELEMENT.pdf`
    1. Shallow water tests are in [shallowWater](/images/Simulations/meshX/shallowWater)
        1. The convergence plots **time step** vs **error** are in [convergence](/images/Simulations/meshX/shallowWater/convergence) and are denoted by `stab_comparison_1D_nlsw_TIME_ELEMENT_STABILIZATION.pdf`
        1. The convergence plots **computational time** vs **error** are in [timeErrorPlot](/images/Simulations/meshX/shallowWater/timeErrorPlot) and files comparing the stabilization techniques are denoted by `2D_meshX_timeError_nlsw_TIME_ELEMENT.pdf`

